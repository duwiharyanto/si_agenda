  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  date_default_timezone_set("Asia/Jakarta");
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard Agenda 
          <small><?php echo  date("h:i:s a");?></small>
        </h1>
<!--         <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol> -->
        
        <marquee><h3 class="text-danger">Tanggal : <?php echo date('d-m-Y');?></h3></marquee>
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>3</h3>
              <p>Agenda hari ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>        
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo $jum_user ?></h3>

                <p>User Terdaftar</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>          
        </div>
        <div class="row">
          <div class="col-sm-2 col-xs-12">
            <a href="<?php echo site_url('Agenda/tambah')?>" class="btn btn-block btn-primary btn-flat"><span class="fa fa-calendar"></span> Tambah</a>        
          </div>
        </div>
        <br>
        <div class="row">        
          <div class="col-sm-12">
            <?php 
              //echo $this->session->flashdata('simpan');
              if(!empty($this->session->flashdata('login'))){
               ?>
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Perhatian!</h4>
                      <?php echo  $this->session->flashdata('login')?>
                  </div>               
               <?php 
              }elseif(!empty($this->session->flashdata('simpan'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil ditambahkan.
                  </div>               
               <?php                
              }elseif(!empty($this->session->flashdata('update'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil dirubah.
                  </div>               
               <?php                
              }elseif(!empty($this->session->flashdata('hapus'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                     <?php echo $this->session->flashdata('hapus')?>
                  </div>               
               <?php   
              } 
            ?>                 
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Agenda</h3>
            </div>
            <!-- /.box-header -->
              <div class="box-body table-responsive ">
                <div class="">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th width="150px">Unit</th>
                    <th width="150px">Username</th>
                    <th width="200px">Password</th>
                    <th>Level</th>
                    <th style="text-align:center;" width="60px">Option</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($user_data as $res) {
                      echo "<tr>";
                      echo "<td>".$res->unit."</td>";
                       echo "<td>".$res->username."</td>";
                      echo "<td>".$res->password."</td>";
                      echo "<td>".ucwords($res->level)."</td>";
                      echo "<td align='center'><a href='".site_url('Agenda/user_detail/'.$res->id)."' class='btn btn-sm btn-flat btn-primary'><i class='fa fa-pencil '></i></a>
                      <a href='".site_url('Admin/user_hapus/'.$res->id)."' class='btn btn-sm btn-flat btn-danger delete-link'><i class='fa fa-trash'></i></a>
                      </td>";
                      echo "</tr>";
                    }
                  ?>
                  </tbody>
                </table>                  
                </div>
              </div>
            <!-- box-body -->
            </div>
            <!-- box -->             
          </div>      
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>
