  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  date_default_timezone_set("Asia/Jakarta");
  ?>
  <script>
  setInterval(
     function(){
     $.get("<?php echo base_url('Agenda/realtime')?>", function(Jam){
              var xJam = Jam;
              var x = document.getElementById('tempat_jam');
                    x.innerHTML = xJam;
        });
     },1000);
</script>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Agenda 
         <!--  <small><?php echo  date("h:i:s a");?></small> -->
        </h1>
<!--         <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol> -->
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3 id="tempat_jam"></h3>

                <p>Tanggal : <?php echo date('d-m-Y');?></p>
              </div>
              <div class="icon">
                <i class="fa  fa-bullhorn"></i>
              </div>
              <a href="#" class="hide small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo $agenda_hariini?></h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="fa fa-calendar"></i>
              </div>
             <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
          </div>                    
        </div>
        <div class="row hidden-md hidden-lg">
          <div class="col-sm-12 col-md-2 col-xs-12 ">
            <a href="<?php echo site_url('Agenda/tambah')?>" class="btn btn-block btn-primary btn-flat"><span class="fa fa-calendar"></span> Tambah</a>        
          </div>
        </div>
        </br>
        <div class="row">        
          <div class="col-sm-12">
            <?php 
              //echo $this->session->flashdata('simpan');
              if(!empty($this->session->flashdata('login'))){
               ?>
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Perhatian!</h4>
                      <?php echo  $this->session->flashdata('login')?>
                  </div>               
               <?php 
              }elseif(!empty($this->session->flashdata('simpan'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil ditambahkan.
                  </div>               
               <?php                
              }elseif(!empty($this->session->flashdata('update'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil dirubah.
                  </div>               
               <?php                
              }
            ?>          
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Agenda</h3>
              <div class="box-tools hidden-xs hidden-sm ">
                <a href="<?php echo site_url('Agenda/tambah')?>" class="btn btn-block btn-primary btn-flat"><span class="fa fa-calendar"></span> Tambah</a>        
              </div>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th width="80px" class="hide">Sort</th>
                    <th width="150px">Tanggal</th>
                    <th width="150px">Unit</th>
                    <th width="200px">Ditujukan Ke</th>
                    <th>Tempat</th>
                    <th>Pembahasan</th>
                    <th style="text-align:center;" width="60px">Option</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($agenda_data as $res) {
                      echo "<tr class='";if($res->selisih < 0){echo "hide";};echo"'>";
                        echo "<td class='hide'>".$res->tanggal."</td>";
                        echo "<td>";if($res->selisih==0){echo "<i class='label label-danger'> Hari ini</i><br> ";}; echo date('d-m-Y',strtotime($res->tanggal))."<br>".$res->jam;echo "</td>";
                        echo "<td>".$res->unit."</td>";
                        echo "<td>".$res->ditujukan."</td>";
                        echo "<td>".ucwords($res->tempat)."</td>";
                        echo "<td>".ucwords($res->agenda)."</td>";
                        echo "<td align='center'><a href='".site_url('Agenda/agenda_detail/'.$res->id)."' class='btn btn-sm btn-flat btn-primary'><i class='fa fa-info'></i></a></td>";
                      echo "</tr>";
                    }
                  ?>
                  </tbody>
                </table>                  
                </div>
              </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->        
          </div>
          <div>
            
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>