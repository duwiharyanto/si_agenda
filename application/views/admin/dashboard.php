  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  date_default_timezone_set("Asia/Jakarta");
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard Agenda 
          <small><?php echo  date("h:i:s a");?></small>
        </h1>
<!--         <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol> -->
        
        <marquee><h3 class="text-danger">Tanggal : <?php echo date('d-m-Y');?></h3></marquee>
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $jum_agenda_hariini?></h3>
              <p>Agenda hari ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>        
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo $jum_user ?></h3>

                <p>User Terdaftar</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>          
        </div>
        <div class="row">
          <div class="col-sm-2 col-xs-12">
            <a href="<?php echo site_url('Agenda/tambah')?>" class="btn btn-block btn-primary btn-flat"><span class="fa fa-calendar"></span> Tambah</a>        
          </div>
        </div>
        <br>
        <div class="row">        
          <div class="col-sm-12">
            <?php 
              //echo $this->session->flashdata('simpan');
              if(!empty($this->session->flashdata('login'))){
               ?>
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Perhatian!</h4>
                      <?php echo  $this->session->flashdata('login')?>
                  </div>               
               <?php 
              }elseif(!empty($this->session->flashdata('simpan'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil ditambahkan.
                  </div>               
               <?php                
              }elseif(!empty($this->session->flashdata('update'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data Berhasil dirubah.
                  </div>               
               <?php                
              }elseif(!empty($this->session->flashdata('hapus'))){
                // echo $this->session->flashdata('simpan');
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                     <?php echo $this->session->flashdata('hapus')?>
                  </div>               
               <?php   
              } 
            ?>                 
          </div>
        </div>
        <div class="row">
          <div class="col-sm-8 col-xs-12">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Agenda</h3>
            </div>
            <!-- /.box-header -->
              <div class="box-body table-responsive ">
                <div class="">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th width="80px">Tanggal</th>
                    <th width="150px">Unit</th>
                    <th width="200px">Ditujukan Ke</th>
                    <th>Tempat</th>
                    <th>Pembahasan</th>
                    <th style="text-align:center;" width="60px">Option</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($agenda_data as $res) {
                      echo "<tr>";
                      echo "<td>".date('d-m-Y',strtotime($res->tanggal))."<br>".$res->jam."</td>";
                       echo "<td>".$res->unit."</td>";
                      echo "<td>".$res->ditujukan."</td>";
                      echo "<td>".ucwords($res->tempat)."</td>";
                       echo "<td>".ucwords($res->agenda)."</td>";
                      echo "<td align='center'><a href='".site_url('Agenda/agenda_detail/'.$res->id)."' class='btn btn-sm btn-flat btn-primary'><i class='fa fa-info'></i></a>
                      <a href='".site_url('Admin/agenda_hapus/'.$res->id)."' class='btn btn-sm btn-flat btn-warning'><i class='fa fa-trash'></i></a>
                      </td>";
                      echo "</tr>";
                    }
                  ?>
                  </tbody>
                </table>                  
                </div>
              </div>
            <!-- box-body -->
            </div>
            <!-- box -->             
          </div>
          <div class="col-sm-4 col-xs-12">
          <!-- PRODUCT LIST -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Pengguna</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                <?php
                  foreach ($user as $res) {
                    ?>
                    <li class="item">
                      <div class="product-img">
                        <img src="<?php echo base_url()?>asset/dist/img/avatar6.png" alt="Product Image">
                      </div>
                      <div class="product-info">
                        <a href="javascript:void(0)" class="product-title"> Username :<?php echo $res->username?>
                        </a>
                        <a href="<?php echo site_url('Admin/user_hapus/'.$res->id)?>" class="delete-link pull-right btn btn-danger"><i class="fa fa-trash"></i></a>
                            <span class="product-description">
                              Unit :<?php echo $res->unit?>
                            </span>
                      </div>
                    </li>                    
                    <?php
                  }
                ?>

                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?php echo site_url('Admin/daftar_user');?>" class="uppercase">Lihat Semua</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->            
          </div>        
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>
  <script>
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Perhatian',
                        text: 'Hapus Data ?',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>