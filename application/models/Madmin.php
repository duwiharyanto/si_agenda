<?php
	class Madmin extends CI_Model{

		private $dbpmb;
		public function __construct(){
			parent::__construct();
			// $this->dbpmb=$this->load->database('PMB',TRUE);
		}
		function get_user(){
			$query=$this->db->query("SELECT * FROM tb_user ORDER BY id DESC LIMIT 5 ");	
			return $query;
		}
		function get_all_user(){
			$query=$this->db->query("SELECT * FROM tb_user");	
			return $query;
		}	
		function user_hapus($id){
			$this->db->where('id',$id);
			$this->db->delete('tb_user');
			return true;
		}	
		function agenda_hapus($id){
			$this->db->where('id',$id);
			$this->db->delete('tb_agenda');
			return true;
		}
		function get_jurusan(){
			$query=$this->db->query("SELECT * FROM tb_daftarjurusan");
			return $query;
		}
		function simpan_user($data){
			$this->db->insert('tb_user',$data);
			return true;
		}
		function cek_login($username,$password){
			$query=$this->db->query("SELECT * from tb_user WHERE username='$username' and password='$password'");
			return $query;
		}
		function simpan_agenda($data){
			$this->db->insert('tb_agenda',$data);
			return true;
		}
		function get_data_agenda(){
			$query=$this->db->query("SELECT
				tb_agenda.id,
				tb_agenda.iduser,
				tb_agenda.ditujukan,
				tb_agenda.tempat,
				tb_agenda.tanggal,
				tb_agenda.jam,
				tb_agenda.agenda,
				tb_agenda.pembahasan,
				tb_user.unit,
				tb_user.username
				FROM
				tb_agenda
				LEFT JOIN tb_user ON tb_agenda.iduser = tb_user.id ORDER BY tb_agenda.id DESC
				");
			return $query;
		}
		function get_detail_agenda($id){
			$query=$this->db->query("SELECT
				tb_agenda.id,
				tb_agenda.iduser,
				tb_agenda.ditujukan,
				tb_agenda.tempat,
				tb_agenda.tanggal,
				tb_agenda.jam,
				tb_agenda.agenda,
				tb_agenda.pembahasan,
				tb_user.unit,
				tb_user.username
				FROM
				tb_agenda
				LEFT JOIN tb_user ON tb_agenda.iduser = tb_user.id WHERE tb_agenda.id='$id'
				");
			return $query->row();
		}
		function agenda_update($id,$data){
			$this->db->where('id',$id);
			$this->db->update('tb_agenda',$data);
			
			return true;
		}
	}
?>