
  <!-- Full Width Column -->  <?php 
  require_once(APPPATH.'views\gui\header.php');
  require_once(APPPATH.'views\gui\menu.php');
  ?>
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="callout callout-info">
          <h4>Tip!</h4>

          <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a
            sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular
            links instead.</p>
        </div>
        <div class="row">
        <!-- col for jurusan -->
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Pilih Jurusan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                  <tr>
                    <th style="width: 120px">Per Jurusan</th>
                    <th>Fak/Jurusan/Prodi/Jenjang</th>
                  </tr>
                  <tr>
                    <td style="font-size:20px;"><a href="<?php echo site_url('Administrator/data')?>">01</a></td>
                    <td>Teknologi Industri/Teknik Kimia/Teknik Kimia/S-1</td>
                  </tr>
                  <tr>
                     <td style="font-size:20px;"><a href="#">02</a></td>
                    <td>Teknologi Industri/Teknik Industri/Teknik Industri/S-1</td>
                  </tr>
                  <tr>
                     <td style="font-size:20px;"><a href="#">03</a></td>
                    <td>Teknologi Industri/Teknik Mesin/Teknik Mesin/S-1</td>
                  </tr>
                  <tr>
                     <td style="font-size:20px;"><a href="#">04</a></td>
                    <td>Teknologi Industri/Teknik Elektro/Teknik Elektro/S-1</td>
                  </tr>
                  <tr>
                     <td style="font-size:20px;"><a href="#">05</a></td>
                    <td>Teknologi Industri/Teknik Informatika/Teknik Informatika/S-1</td>
                  </tr>
                  <tr>
                     <td style="font-size:20px;"><a href="#">06</a></td>
                    <td>Sains Terapan/Statistika/Statistika/S-1</td>
                  </tr>
                  <tr>
                    <td style="font-size:20px;"><a href="#">07</a></td>
                    <td>Sains Terapan/Sistem Komputer/Sistem Komputer/S-1</td>
                  </tr>
                  <tr>
                    <td style="font-size:20px;"><a href="#">08</a></td>
                    <td>Sains Terapan/Fisika/Fisika/S-1</td>
                  </tr>
                  <tr>
                    <td style="font-size:20px;"><a href="#">09</a></td>
                    <td>Sains Terapan/Kimia/Ilmu Kimia/S-1</td>
                  </tr>
                  <tr>
                    <td style="font-size:20px;"><a href="#">10</a></td>
                    <td>Teknologi Mineral/Teknik Geologi/Teknik Geologi/S-1</td>
                  </tr> 
                  <tr>
                    <td style="font-size:20px;"><a href="#">11</a></td>
                    <td>Sains Terapan/Teknik Lingkungan/Teknik Lingkungan/S-1</td>
                  </tr>                                                                                                                                                      
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!--  col for newest-->
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Upload Terbaru</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th>File</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Nama Uploader</a></td>
                    <td>Call of Duty IV</td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Nama Upload</a></td>
                    <td>Samsung Smart TV</td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Nama Upload</a></td>
                    <td>iPhone 6 Plus</td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->          
        </div>        
        <!--  ./col-->
        </div>


        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
<?php
  require_once(APPPATH.'views\gui\footer.php');
?>