  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content">
        <div class="row">
            <?php 
              //echo $this->session->flashdata('simpan');
              if(!empty($this->session->flashdata('simpan'))){
               ?>
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      Data berhasil disimpan, silahkan <a href="<?php echo site_url('Agenda/login')?>">Login disini</a>
                  </div>               
               <?php 
              }
            ?>
            <!-- LOGIN BOX -->
            <div class="login-box">
              <div class="login-logo">
                <a href="<?php echo site_url(); ?>"><span class="fa fa-user"> </span>Register</a>
              </div>
              <!-- /.login-logo -->
              <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="<?php echo site_url('Agenda/simpan_user');?>" method="post">
                  <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="unit" name="unit" style="text-transform:uppercase;">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>                
                  <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="username" name="username">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <button type="submit" class="btn btn-success btn-block btn-flat">Daftar</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>
              </div>
              <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>