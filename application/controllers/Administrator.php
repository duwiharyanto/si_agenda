<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// $this->load->view('gui/header');
		// $this->load->view('gui/menu');
		$this->load->view('body/jurusan');
		// $this->load->view('gui/footer');
		// echo "hello world";
	}
	public function data(){
		//$this->load->view('gui/header');
		//$this->load->view('gui/menu');
		$this->load->view('body/data');
		//$this->load->view('gui/footer');

	}
}
