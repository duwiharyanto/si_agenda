<?php
	class Magenda extends CI_Model{

		private $dbpmb;
		public function __construct(){
			parent::__construct();
			// $this->dbpmb=$this->load->database('PMB',TRUE);
		}

		function get_jurusan(){
			$query=$this->db->query("SELECT * FROM tb_daftarjurusan");
			return $query;
		}
		function simpan_user($data){
			$this->db->insert('tb_user',$data);
			return true;
		}
		function cek_login($username,$password){
			$query=$this->db->query("SELECT * from tb_user WHERE username='$username' and password='$password'");
			return $query;
		}
		function simpan_agenda($data){
			$this->db->insert('tb_agenda',$data);
			return true;
		}
		function get_data_agenda(){
			$query=$this->db->query("SELECT
				tb_agenda.id,
				tb_agenda.iduser,
				tb_agenda.ditujukan,
				tb_agenda.tempat,
				tb_agenda.tanggal,
				tb_agenda.jam,
				tb_agenda.agenda,
				tb_agenda.pembahasan,
				tb_user.unit,
				tb_user.username
				FROM
				tb_agenda
				LEFT JOIN tb_user ON tb_agenda.iduser = tb_user.id ORDER BY tb_agenda.id DESC
				");
			return $query;
		}
		function get_detail_agenda($id){
			$query=$this->db->query("SELECT
				tb_agenda.id,
				tb_agenda.iduser,
				tb_agenda.ditujukan,
				tb_agenda.tempat,
				tb_agenda.tanggal,
				tb_agenda.jam,
				tb_agenda.agenda,
				tb_agenda.pembahasan,
				tb_user.unit,
				tb_user.username
				FROM
				tb_agenda
				LEFT JOIN tb_user ON tb_agenda.iduser = tb_user.id WHERE tb_agenda.id='$id'
				");
			return $query->row();
		}
		function agenda_update($id,$data){
			$this->db->where('id',$id);
			$this->db->update('tb_agenda',$data);
			
			return true;
		}
		//Query mencari data agenda aktif hari N
		function get_data_selisih_agenda(){
			$query=$this->db->query("SELECT
				tb_agenda.id,
				tb_agenda.iduser,
				tb_agenda.ditujukan,
				tb_agenda.tempat,
				tb_agenda.tanggal,
				tb_agenda.jam,
				tb_agenda.agenda,
				tb_agenda.pembahasan,
				tb_user.unit,
				tb_user.username,
				datediff(tb_agenda.tanggal,current_date()) as selisih
				FROM
				tb_agenda
				LEFT JOIN tb_user ON tb_agenda.iduser = tb_user.id ORDER BY tb_agenda.tanggal DESC
				");
			return $query;
		}
		function get_data_agenda_harini(){
			$query=$this->db->query("SELECT id, iduser,tanggal, datediff(tanggal,current_date()) as 'selisih' FROM tb_agenda where  datediff(tanggal,current_date()) = 0
				");
			return $query;
		}		
	}
?>