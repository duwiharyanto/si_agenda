  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content">
        <div class="row">
            <!-- LOGIN BOX -->
            <div class="col-sm-6 col-sm-offset-3">
              <div class="login-logo">
                <a href="<?php echo site_url(); ?>"><span class="fa fa-calendar"> </span>Detail Agenda</a>
              </div>
              <!-- /.login-logo -->
              <div class="login-box-body">
                <div class="">
                <marquee><h4 class="login-box-msg ">Tanggal : <?php echo date('d-m-Y');?></h4></marquee>
                </div>
                <form action="<?php echo site_url('Agenda/agenda_update')?>" method="post">
                  <div class="form-group has-feedback hide">
                    <label>Id Agenda</label>
                    <input type="text" class="form-control" placeholder="unit" name="idagenda" readonly="" value="<?php echo $agenda_detail->id?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>                
                  <div class="form-group has-feedback hide">
                    <label>Id USer</label>
                    <input type="text" class="form-control" placeholder="unit" name="iduser" readonly="" value="<?php echo $agenda_detail->iduser?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>                 
                  <div class="form-group has-feedback">
                    <label>Unit</label>
                    <input type="text" class="form-control" placeholder="unit" name="unit" readonly="" value="<?php echo $agenda_detail->unit?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>  
                  <div class="form-group has-feedback">
                    <label>Undangan ke </label>
                    <input type="text" class="form-control" placeholder="Undangan Ke" name="undangan" value="<?php echo $agenda_detail->ditujukan?>" <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "readonly=''";}?>>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <label>Tempat</label>
                    <input type="text" class="form-control" placeholder="Tempat" name="tempat" value="<?php echo $agenda_detail->tempat?>" <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "readonly=''";}?>>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>                                                     
                  <div class="form-group has-feedback">
                    <label>Tanggal</label>
                    <input type="text" class="form-control datepicker" placeholder="Tanggal" name="tanggal" value="<?php echo date('d-m-Y',strtotime($agenda_detail->tanggal))?>" <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "readonly=''";}?>>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                  </div>
                  <div class="bootstrap-timepicker">
                  <div class="form-group has-feedback">
                    <label>Jam</label>
                    <input type="text" class="form-control timepicker" placeholder="Jam" name="jam" value="<?php echo $agenda_detail->jam?>" <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "readonly=''";}?>>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  </div>
                  <div class="form-group has-feedback">
                    <label>Agenda</label>
                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="agenda" <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "readonly=''";}?>><?php echo $agenda_detail->agenda?></textarea>
                  </div>                  
                  <div class="row">
                    <div class="col-xs-12">
                      <a href="<?php echo site_url()?>" class="btn btn-success btn-block btn-flat">Kembali</a>
                      <button type="submit" class="btn btn-primary btn-block btn-flat <?php if($this->session->userdata('id')!=$agenda_detail->iduser){echo "hide";}?>">Update</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>
              </div>
              <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->         </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>
