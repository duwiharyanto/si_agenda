  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content">
        <div class="row">
            <!-- LOGIN BOX -->
            <div class="login-box">
            <?php
              if($this->session->flashdata('login')==true){
               ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
                      <?php echo $this->session->flashdata('login') ?>
                  </div>               
               <?php
              }
            ?>
              <div class="login-logo">
                <a href="<?php echo site_url(); ?>"><span class="fa fa-lock"> </span>Login User</a>
              </div>
              <!-- /.login-logo -->
              <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="<?php echo site_url('Agenda/login')?>" method="post">
                  <div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value('username')?>">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <p class="text-red"><?php  echo form_error('username');?></p>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo set_value('password')?>">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <p class="text-red"><?php  echo form_error('password');?></p>
                  </div>
                  <div class="row">
                    <div class="col-xs-12 col-sm-4">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                      <a href="<?php echo site_url('Agenda/daftar')?>" class="hidden-lg btn btn-warning btn-block btn-flat">Daftar</a>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>
                <br>
                <a href="#">Lupa Password</a><br>
                <a href="<?php echo site_url('Agenda/daftar')?>" class="text-center hidden-xs">Daftar User</a>
              </div>
              <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>