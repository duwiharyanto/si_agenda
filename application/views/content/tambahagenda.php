  <?php 
  require_once(APPPATH.'views/gui/header.php');
  require_once(APPPATH.'views/gui/menu.php');
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content">
        <div class="row">
            <!-- LOGIN BOX -->
            <div class="col-sm-6 col-sm-offset-3">
              <div class="login-logo">
                <a href="<?php echo site_url(); ?>"><span class="fa fa-calendar"> </span>Tambah Agenda</a>
              </div>
              <!-- /.login-logo -->
              <div class="login-box-body">
                <div class="">
                <marquee><h4 class="login-box-msg ">Tanggal : <?php echo date('d-m-Y');?></h4></marquee>
                </div>
                <form action="<?php echo site_url('Agenda/simpan_agenda')?>" method="post">
                  <div class="form-group has-feedback">
                    <label>Unit</label>
                    <input type="text" class="form-control" placeholder="unit" name="unit" readonly="" value="<?php echo $this->session->userdata('unit')?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>  
                  <div class="form-group has-feedback">
                    <label>Undangan ke </label>
                    <input type="text" class="form-control" placeholder="Undangan Ke" name="undangan">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <label>Tempat</label>
                    <input type="text" class="form-control" placeholder="Tempat" name="tempat">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>                                                     
                  <div class="form-group has-feedback">
                    <label>Tanggal</label>
                    <input type="text" class="form-control datepicker" placeholder="Tanggal" name="tanggal">
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                  </div>
                  <div class="bootstrap-timepicker">
                  <div class="form-group has-feedback">
                    <label>Jam</label>
                    <input type="text" class="form-control timepicker" placeholder="Jam" name="jam">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  </div>
                  <div class="form-group has-feedback">
                    <label>Agenda</label>
                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="agenda"></textarea>
                  </div>                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-4">
                      <button type="submit" class="btn btn-success btn-block btn-flat">Simpan</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>
              </div>
              <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->         </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  require_once(APPPATH.'views/gui/footer.php');
  ?>