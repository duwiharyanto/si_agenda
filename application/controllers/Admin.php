<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model(array('Magenda','Madmin'));
			/*
			if($this->session->userdata('status') != "login"){
				redirect(base_url('clogin'));
			}
			*/	
	}	
	public function index()
	{
		$data['agenda_data']=$this->Magenda->get_data_agenda()->result();
		$data['user']=$this->Madmin->get_user()->result();
		$data['jum_user']=$this->Madmin->get_all_user()->num_rows();
		$data['jum_agenda_hariini']=$this->Magenda->get_data_agenda_harini()->num_rows();
		$this->load->view('admin/dashboard',$data);
	}
	public function agenda_hapus($id){
		$res=$this->Madmin->agenda_hapus($id);
		if($res==true){
			$this->session->set_flashdata('hapus','Data berhasil dihapus');
			redirect('Admin');
		}
	}
	public function user_hapus($id){
		$res=$this->Madmin->user_hapus($id);
		if($res==true){
			$this->session->set_flashdata('hapus','Pengguna berhasil dihapus');
			redirect('Admin');
		}
	}
	public function daftar_user(){
		$data=array(
			'user_data'=>$this->Madmin->get_all_user()->result(),
			'jum_user'=>$this->Madmin->get_all_user()->num_rows(),
			);
		$this->load->view('admin/daftar_user',$data);		
	}
	public function user_edit(){
		
	}
}
