<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model(array('Magenda'));
			/*
			if($this->session->userdata('status') != "login"){
				redirect(base_url('clogin'));
			}
			*/	
	}	
	public function realtime(){
		$this->load->view('content/realtime');
	}
	public function index()
	{
		$data['agenda_data']=$this->Magenda->get_data_selisih_agenda()->result();
		$data['agenda_hariini']=$this->Magenda->get_data_agenda_harini()->num_rows();
		$this->load->view('content/home',$data);
	}
	public function data(){
		//$this->load->view('gui/header');
		//$this->load->view('gui/menu');
		$this->load->view('body/data');
		//$this->load->view('gui/footer');

	}
	public function login(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()==true){
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$cek=$this->Magenda->cek_login($username,$password);
			if($cek->num_rows() == 1){
				foreach ($cek->result() as $userdata) {
					$session_data['id']=$userdata->id;
					$session_data['unit']=$userdata->unit;
					$session_data['username']=$userdata->username;
					$session_data['level']=$userdata->level;
					$session_data['status_login']='login';
					$this->session->set_userdata($session_data);
				}
				if($this->session->userdata('level')=='nonroot'){
					$this->session->set_flashdata('login','Berhasil Login');
					redirect(site_url('Agenda'));
				}else{
					redirect(site_url('Admin'));
				}
			}else{
					$this->session->set_flashdata('login','Username dan Password Salah');
					$this->load->view('content/login');
				}
		}else{
			$this->load->view('content/login');
		}
	}
	public function daftar(){
		$this->load->view('content/daftar');
	}
	public function tambah(){
		if($this->session->userdata('status_login') != "login"){
				redirect(base_url('Agenda/login'));
		}else{
			$this->load->view('content/tambahagenda');
		}		
	}
	public function pencarianunit(){
		$this->load->view('content/pencarianunit');
	}
	public function pencariantanggal(){
		$this->load->view('content/pencariantanggal');
	}
	public function pencariantempat(){
		$this->load->view('content/pencariantempat');
	}
	public function simpan_user(){
		$data=array(
			'unit'=>$this->input->post('unit'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			'level'=>'nonroot',	
			);
		$res=$this->Magenda->simpan_user($data);
		if($res){
			$this->session->set_flashdata('simpan','Data berhasil disimpan');
			redirect(site_url('Agenda/daftar'));
		}else{
			$this->session->set_flashdata('simpan','Data gagal disimpan');
		}
	}
	public function proses_login(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()==true){
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$cek=$this->Magenda->cek_login($username,$password);
			if($cek->num_rows() == 1){
				foreach ($cek->result() as $userdata) {
					$session_data['id']=$userdata->id;
					$session_data['unit']=$userdata->unit;
					$session_data['username']=$userdata->username;
					$session_data['level']=$userdata->level;
					$session_data['status_login']='login';
					$this->session->set_userdata($session_data);
				}
				if($this->session->userdata('level')=='nonroot'){
					$this->session->set_flashdata('login','true');
					redirect(site_url('Agenda'));
				}
			}else{
					$this->session->set_flashdata('login','false');
					redirect(site_url('Agenda/login'));
				}
		}else{
			$this->load->view('content/login');
		}

	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url('Agenda/login'));
	}
	public function simpan_agenda(){
		$data=array(
			'iduser'=>$this->session->userdata('id'),
			'ditujukan'=>$this->input->post('undangan'),
			'tempat'=>$this->input->post('tempat'),
			'tanggal'=>date('Y-m-d',strtotime($this->input->post('tanggal'))),
			'jam'=>$this->input->post('jam'),
			'agenda'=>$this->input->post('agenda')
		);
		// echo $data['jam'];
		$res=$this->Magenda->simpan_agenda($data);
		if($res==true){
			$this->session->set_flashdata('simpan',true);
		}else{
			$this->session->set_flashdata('simpan',false);
		}
		redirect(site_url('Agenda'));
	}
	public function agenda_detail($id){
		$data['agenda_detail']=$this->Magenda->get_detail_agenda($id);
		$this->load->view('content/agendadetail',$data);
	}
	public function agenda_update(){
		$id=$this->input->post('idagenda');
		$data=array(
			'ditujukan'=>$this->input->post('undangan'),
			'tempat'=>$this->input->post('tempat'),
			'tanggal'=>date('Y-m-d',strtotime($this->input->post('tanggal'))),
			'jam'=>$this->input->post('jam'),
			'agenda'=>$this->input->post('agenda')
			);
		$res=$this->Magenda->agenda_update($id,$data);
		if($res==true){
			$this->session->set_flashdata('update',true);
			redirect(site_url('Agenda'));
		}else{
			echo 'gagal';
		}
	}
}
