  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo site_url();?>" class="navbar-brand"><span class="fa fa-calendar-check-o "></span> <span class="hidden-lg">Agenda</span></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Agenda <span class="sr-only">(current)</span></a></li>
            <!-- <li><a href="#">Login</a></li> -->
            <li class="dropdown hide">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pencarian <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('Agenda/pencarianunit')?>">Unit</a></li>
                <li><a href="<?php echo site_url('Agenda/pencariantanggal')?>">Tanggal</a></li>
                <li><a href="<?php echo site_url('Agenda/pencariantempat')?>">Tempat</a></li>
                <!-- <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li> -->
              </ul>
            </li>              
          </ul>
        </div>

        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav pull-left  <?php if($this->session->userdata('status_login')=='login'){echo "hide";}?>">
            <li><a href="<?php echo site_url('Agenda/Login')?>"><span class="fa fa-sign-in"></span> Login</a></li>
          </ul>
          <?php
            if($this->session->userdata('status_login')=='login'){
            ?>
              <ul class="nav navbar-nav">            
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                  <!-- Menu Toggle Button -->
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <img src="<?php echo base_url()?>asset/dist/img/avatar6.png" class="user-image" alt="User Image">
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs"><?php echo $this->session->userdata('username')?></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="user-header">
                      <img src="<?php echo base_url()?>asset/dist/img/avatar6.png" class="img-circle" alt="User Image">
                      <p>
                        <?php echo $this->session->userdata('username')?> - <?php echo $this->session->userdata('level')?>
                        
                      </p>
                    </li>
                    <li class="user-footer">
                      <div class="pull-left">
                        <a href="<?php echo site_url('Admin');?>" class="btn btn-default btn-flat <?php if($this->session->userdata('level')!='root'){echo 'hide';}?>">Profile</a>
                      </div>
                      <div class="pull-right">
                        <a href="<?php echo site_url('Agenda/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                      </div>
                    </li>
                  </ul>            
            <?php  
            }
          ?>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>